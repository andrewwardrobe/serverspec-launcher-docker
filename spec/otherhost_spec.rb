require 'spec_helper'

RSpec.context 'Example Serverspec' do
  describe file '/etc/hosts' do
    it { should exist }
    it { should be_a_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }

  end
  describe file '/etc/fstab' do
    it { should exist }
    it { should be_a_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
  end
end
