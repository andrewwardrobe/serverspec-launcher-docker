require 'spec_helper'

RSpec.context 'Example Serverspec' do
  describe file '/etc/hosts' do
    it { should exist }
    it { should be_a_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
  end
  describe file '/etc/fstab' do
    it { should exist }
    it { should be_a_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
  end
end

RSpec.context 'Other Context' do
  describe file '/etc/os-release' do
    it { should exist }
  end
end

RSpec.context 'two level Context' do
  context 'inner Context' do
    describe file '/etc/hosts' do
      it { should exist }
      it { should be_a_file }
      it { should be_owned_by 'root' }
      it { should be_grouped_into 'root' }
    end
  end
end